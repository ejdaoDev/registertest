<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase {

    public $base_uri = 'http://localhost/registertest/server/public/api/';
    public $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3JlZ2lzdGVydGVzdFwvc2VydmVyXC9wdWJsaWNcL2FwaVwvbG9naW4iLCJpYXQiOjE2MzgwNDAzNTksImV4cCI6MTYzODA2MTk1OSwibmJmIjoxNjM4MDQwMzU5LCJqdGkiOiJHcHdoendEY3F4MXVyMURQIiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.nTD5wqzdTcIadS1otC9kob-G1UabZK7LKaoq_jBEwoM';

    use CreatesApplication;
}
