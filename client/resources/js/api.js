
export const url = "http://127.0.0.1:8000/api/";
export const config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("auth.token"),
    },
  };
